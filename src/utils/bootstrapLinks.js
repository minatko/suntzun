import React from "react"
import { Link } from "gatsby"

const NavbarBrand = props => <Link className="navbar-brand" {...props} />
const NavLink = props => <Link className="nav-link" {...props} />

export { NavbarBrand, NavLink }
