import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { useState } from "react"

import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  Container,
} from "reactstrap"

import { NavbarBrand, NavLink } from "../utils/bootstrapLinks"
import { from } from "rxjs"

const Header = ({ siteTitle }) => {
  const [open, setOpen] = useState(false)
  const toggle = () => setOpen(!open)
  return (
    <header>
      <Navbar color="light" light expand="md">
        <Container fluid>
          <NavbarBrand to="/">{siteTitle}</NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={open} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink to="blog">Блог</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="#services">Услуги</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="#price">Прайс-лист</NavLink>
              </NavItem>
            </Nav>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink to="https://twitter.com/Heegiiny">Twitter</NavLink>
              </NavItem>
              <NavItem>
                <NavLink to="https://github.com/Heegiiny">GitHub</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/components/">order@suntzun.com</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
