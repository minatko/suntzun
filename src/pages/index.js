import React from "react"
import { Link } from "gatsby"

import {
  Container,
  Jumbotron,
  Row,
  Col,
  Card,
  CardTitle,
  CardBody,
  CardText,
  CardLink,
  CardImg,
  Table,
} from "reactstrap"

import Layout from "../components/layout"

const Service = ({ title, children }) => (
  <Card>
    <CardImg />
    <CardBody>
      <CardTitle className="h5">{title}</CardTitle>
      <CardText>{children}</CardText>
      <CardLink href="#">Заказать</CardLink>
    </CardBody>
  </Card>
)

const IndexPage = () => (
  <Layout>
    <Container>
      <Jumbotron className="mt-4">
        <h1 class="display-4">Sun Tzun — лучшие сайты за недорого</h1>
        <p class="lead">
          Лендинги, магазины, блоги, форумы, уникальный дизайн и многое
          другое...
        </p>
        <hr class="my-4" />
        <p>В любом стиле мы привыкли добиваться совершентсва.</p>
        <p class="lead">
          <a class="btn btn-primary btn-lg mr-2" href="#" role="button">
            Заказать
          </a>
          <a class="btn btn-secondary btn-md" href="#" role="button">
            Посмотреть примеры
          </a>
        </p>
      </Jumbotron>

      <section id="services">
        <h2 className="text-center mt-4">Услуги</h2>
        <Row>
          <Col>
            <Service title="Вёрстка">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
              finibus arcu facilisis, tempus orci in, ullamcorper arcu. Morbi
              consectetur a mi et malesuada. Proin in eleifend justo.
            </Service>
          </Col>
          <Col>
            <Service title="Дизайн">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
              finibus arcu facilisis, tempus orci in, ullamcorper arcu. Morbi
              consectetur a mi et malesuada. Proin in eleifend justo.
            </Service>
          </Col>
          <Col>
            <Service title="Программирование">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
              finibus arcu facilisis, tempus orci in, ullamcorper arcu. Morbi
              consectetur a mi et malesuada. Proin in eleifend justo.
            </Service>
          </Col>
        </Row>
      </section>

      <section id="price">
        <h2 className="text-center mt-4">Прайс-лист</h2>

        <Row>
          <Col size="6" offset="3">
            <Table striped>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Услуга</th>
                  <th>Стоимость</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Лендинги </td>
                  <td>11990 ₽</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Несложные </td>
                  <td>24990 ₽</td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>Магазины </td>
                  <td>49990 ₽</td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>Профи </td>
                  <td>300 000 ₽</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </section>

      <section id="contacts">
        <h2 className="text-center mt-4">Контакты</h2>
      </section>
    </Container>
  </Layout>
)

export default IndexPage
