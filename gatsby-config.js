/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: `SunTzun`,
    siteUrl: `https://www.suntzun.com`,
    description: `Developer's web site`,
  },
}
